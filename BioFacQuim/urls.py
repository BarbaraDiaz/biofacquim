"""BioFacQuim URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include 
from django.contrib import admin
from django.conf import settings
from django.urls import path
from django.urls import include # new

from apps.database.views import NaturalProductsView
from apps.database.views import homePageView
from apps.database.views import HomeView, ContactView
from apps.statistics.views import StatisticsView
from apps.download.views import DownloadView, DownloadCSV
from apps.browse.views import BrowseView, PlantView, FungusView, PropolisView
from apps.search.views import SearchView
from apps.ADMET.views import ADMETView


urlpatterns = [
    # path('admin/', admin.site.urls),
    # url(r'^$', homePageView, name='home'),
    url(r'^$', HomeView.as_view()),
    url(r'^contact$', ContactView.as_view()),
    url(r'^statistics$', StatisticsView.as_view()),
    url(r'^download$', DownloadView.as_view()),
    url(r'^download_csv$', DownloadCSV.as_view()),
    url(r'^browse$', BrowseView.as_view()),
    url(r'^plant$', PlantView.as_view()),
    url(r'^fungus$', FungusView.as_view()),
    url(r'^propolis$', PropolisView.as_view()),
    url(r'^search$', SearchView.as_view()),
    url(r'^ADMET$', ADMETView.as_view())
]

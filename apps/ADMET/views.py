from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.template import RequestContext
from rest_framework.views import APIView
import os
import glob

class ADMETView(APIView):

    def get(self, request):
        #return HttpResponse('seccion ADMET')
        return render(request, 'ADMET.html')
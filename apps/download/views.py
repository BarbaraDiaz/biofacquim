from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.template import RequestContext
from rest_framework.views import APIView
# from rest_pandas.views import PandasView
from datetime import datetime
import pandas as pd
import numpy as np
import pickle
import os
import glob
import csv
# from .forms import InputForm
# from .models import NaturalProducts
# Create your views here.
def homePageView(request):
    return HttpResponse('Hello, World! Barby es muy chingona')

class DownloadView(APIView):

    def get(self, request):
        return render(request, 'download.html')

class DownloadCSV(APIView):
    def get(self, request):
        # csv_name = request.session['csv_name']
        filename = f'apps/database/csv/BIOFACQUIM.csv'
        with open(filename, 'rb') as csv_file:
            response = HttpResponse(csv_file, content_type="text/csv")
            response['Content-Disposition'] = f'attachment; filename = {filename}'
            return response
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.template import RequestContext
from rest_framework.views import APIView
# from rest_pandas.views import PandasView
from datetime import datetime
import pandas as pd
import numpy as np
import pickle
import os
import glob
import csv
# from .forms import InputForm
from .models import NaturalProducts
# Create your views here.
def homePageView(request):
    return HttpResponse('Hello, World!')

class HomeView(APIView):

    def get(self, request):
        return render(request, 'home.html')

class ContactView(APIView):

    def get(self, request):
        return render(request, 'contact.html')

class NaturalProductsView(object):

    def get_all(self):
        return NaturalProducts.objects.all()

    def filter_by_name(self, name):
        return NaturalProducts.objects.filter(name = name)

###

# # from .forms import InputForm
# # from .compute_sequence import ComputeSequence
# # from apps.PCA.compute_pca import GeneratePCA



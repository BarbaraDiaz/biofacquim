import csv
from django.db import models
from django.db.utils import DataError


# Create your models here.
class NaturalProducts(models.Model):
    '''
    CREATE TABLE NaturalProducts    (
        id              VARCHAR(7) UNIQUE NOT NULL PRIMARY KEY
        name            VARCHAR(50) NOT NULL
        reference       VARCHAR(100) NOT NULL
        year            VARCHAR(50) NOT NULL
        kingdom         VARCHAR(50) NOT NULL
        genus            VARCHAR(50) NOT NULL
        species          VARCHAR(50) NOT NULL
        smiles           VARCHAR(50) NOT NULL
        doi              VARCHAR(50) NOT NULL
        journal          VARCHAR(50) NOT NULL
        site             VARCHAR(50) NOT NULL
        state            VARCHAR(50) NOT NULL
        ic50_1           VARCHAR(50) 
        Activity_1          VARCHAR(50) 
        ic50_2           VARCHAR(50) 
        Activity_2       VARCHAR(50) 
        ic50_3           VARCHAR(50) 
        Activity_3           VARCHAR(50) 
        ic50_4           VARCHAR(50) NOT 
        Activity_4           VARCHAR(50) 
        ic50_5           VARCHAR(50) NOT 
        Activity_5           VARCHAR(50) 
        ic50_6           VARCHAR(50) NOT 
        Activity_6           VARCHAR(50) 
        Bioactivity              VARCHAR(50) 
        Bioactivity2         VARCHAR(50) 
        Bioactivity3         VARCHAR(50) 

    )
    '''
    id          = models.CharField(max_length=500, primary_key=True) 
    name        = models.CharField(max_length = 500, null = False)
    reference   = models.CharField( max_length = 500, null = False)
    year        = models.CharField( max_length = 500, null = False)
    kingdom     = models.CharField( max_length = 500, null = False)
    genus       = models.CharField( max_length = 500, null = False)       
    species     = models.CharField( max_length = 500, null = False)      
    smiles      = models.CharField( max_length = 500, null = False)      
    doi         = models.CharField( max_length = 500, null = False)      
    journal     = models.CharField( max_length = 500, null = False)     
    site        = models.CharField( max_length = 500, null = False)
    state       = models.CharField( max_length = 500, null = False)
    ic50_1      = models.CharField( max_length = 500, null = False)
    Activity_1  = models.CharField( max_length = 500, null = False)
    ic50_2      = models.CharField( max_length = 500, null = False)
    Activity_2  = models.CharField( max_length = 500, null = False)     
    ic50_3      = models.CharField( max_length = 500, null = False)     
    Activity_3  = models.CharField( max_length = 500, null = False)         
    ic50_4      = models.CharField( max_length = 500,  null = False)     
    Activity_4  = models.CharField( max_length = 500,  null = False)        
    ic50_5      = models.CharField( max_length = 500,  null = False)     
    Activity_5  = models.CharField( max_length = 500,  null = False)
    ic50_6      = models.CharField( max_length = 500,  null = False)
    Activity_6  = models.CharField( max_length = 500, null = False)
    Bioactivity = models.TextField(   null = False)
    Bioactivity2= models.TextField(   null = False)
    Bioactivity3 = models.TextField(  null = False)
    
    @classmethod
    def populate(cls):
        data = csv.DictReader(open('apps/database/csv/BIOFACQUIM.csv'))
        for row in data:
            # print(row['Bioactivity(3)'])
            try:
                cls.objects.create(
                    id = row['ID'], 
                    name = row['Name'], 
                    reference = row['Reference'],
                    year = row['Year'],
                    kingdom = row['Kingdom'],
                    genus = row['Genus'],
                    species = row['Specie'],
                    smiles = row['SMILES'],
                    doi = row['DOI'],
                    journal = row['Journal'],
                    site = row['Site'],
                    state = row['State'],
                    ic50_1 = row['IC50_1'],
                    Activity_1 = row['Activity_1'],
                    ic50_2 =row['IC50_2'],
                    Activity_2 = row['Activity_2'],
                    ic50_3 = row['IC50_3'],
                    Activity_3 = row['Activity_3'],
                    ic50_4 = row['IC50_4'],
                    Activity_4 = row['Activity_4'],
                    ic50_5 = row['IC50_5'],
                    Activity_5 = row['Activity_5'],
                    ic50_6 = row['IC50_6'],
                    Activity_6 = row['Activity_6'],
                    Bioactivity = row['Bioactivity'],
                    Bioactivity2 = row['Bioactivity(2)'],
                    Bioactivity3 = row['Bioactivity(3)'],
                    )
            except DataError as e:
                print(e)
            

    @classmethod
    def clean_table(cls):
        cls.objects.all().delete()
        
import pandas as pd
import numpy as np
from numpy import pi
# import pickle
from bokeh.io import output_file
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.models import LassoSelectTool, ZoomInTool, ZoomOutTool,Legend, PanTool, SaveTool, HoverTool
from bokeh.models import ColumnDataSource, ranges, LabelSet
from bokeh.core.enums import LegendLocation

class GenerateStatistics:

    def compute_statistics(self):

        
        Years = ['2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017']
        counts = [11, 14, 29,26,29,25,6,17,25,2,8,16,20,15,33,43,34,61]
        source = ColumnDataSource(data=dict(Years=Years, counts=counts))

        p = figure(x_range=Years, y_range=(0,80), plot_width = 1000, plot_height = 600, title="Compounds Isolated per Year",
                x_axis_label = "Year", y_axis_label="Number of compounds", toolbar_location=None, tools="")

        labels = LabelSet(x='Years', y='counts', text='counts',  x_offset=-8, y_offset=0, source=source)
        p.vbar(x='Years', top='counts',width=0.8,  color='yellowgreen',  source=source)
        p.xgrid.grid_line_color = None
        p.xaxis.axis_label_text_font_size = "20pt"
        p.yaxis.axis_label_text_font_size = "20pt"
        p.xaxis.major_label_text_font_size = "14pt"
        p.yaxis.major_label_text_font_size = "18pt"
        p.title.text_font_size = "22pt"
        p.add_layout(labels)
        ###Second plot
       
        data_TSNE = pd.read_csv('apps/database/csv/Resultados_TSNE.csv')
        data_TSNE = data_TSNE.set_index('TIPO')
        
        #1 natx
        NATx = ["NATx",]
        properties = ["PC1","PC2","LIBRARY","SMILES","ID"]
        df_NATx = []
        for col in properties:
            for row in NATx:
                df_NATx.append([col, row, data_TSNE.at[row, col]]) 

        X_NATx = list(df_NATx[0][2])
        Y_NATx= list(df_NATx[1][2])
        S_NATx= list(df_NATx[3][2])
        N_NATx= list(df_NATx[4][2])
#         print(df_NATx[2][2])

        #2 MARINES
        Marines = ["Marines",]
        df_Marines = []
        for col in properties:
            for row in Marines:
                df_Marines.append([col, row, data_TSNE.at[row, col]]) 
        X_Marines = list(df_Marines[0][2])
        Y_Marines = list(df_Marines[1][2])
        S_Marines= list(df_Marines[3][2])
        N_Marines= list(df_Marines[4][2])

              
        #3 MEGx
        MEGx = ["MEGx",]
        df_MEGx = []
        for col in properties:
            for row in MEGx:
                df_MEGx.append([col, row, data_TSNE.at[row, col]]) 
        X_MEGx = list(df_MEGx[0][2])
        Y_MEGx = list(df_MEGx[1][2])
        S_MEGx= list(df_MEGx[3][2])
        N_MEGx= list(df_MEGx[4][2])
               
        #4 NuBBE 
        NuBBE = ["NuBBE",]
        df_NuBBE = []
        for col in properties:
            for row in NuBBE:
                df_NuBBE.append([col, row, data_TSNE.at[row, col]]) 

        X_NuBBE = list(df_NuBBE[0][2])
        Y_NuBBE= list(df_NuBBE[1][2])
        S_NuBBE= list(df_NuBBE[3][2])
        N_NuBBE= list(df_NuBBE[4][2])
        
        #5 Approved
        Approved = ["Approved",]
        df_Approved = []
        for col in properties:
            for row in Approved:
                df_Approved.append([col, row, data_TSNE.at[row, col]]) 
        X_Approved = list(df_Approved[0][2])
        Y_Approved = list(df_Approved[1][2])
        S_Approved= list(df_Approved[3][2])
        N_Approved= list(df_Approved[4][2])
        
        #6 Cyanobacterias
        Cyanobacterias = ["Cyanobacterias",]
        df_Cyanobacterias = []
        for col in properties:
            for row in Cyanobacterias:
                df_Cyanobacterias.append([col, row, data_TSNE.at[row, col]]) 
        X_Cyanobacterias = list(df_Cyanobacterias[0][2])
        Y_Cyanobacterias = list(df_Cyanobacterias[1][2])
        S_Cyanobacterias= list(df_Cyanobacterias[3][2])
        N_Cyanobacterias= list(df_Cyanobacterias[4][2])

        #7 BioFacQuim
        BioFacQuim = ["BioFacQuim",]
        df_BioFacQuim = []
        for col in properties:
            for row in BioFacQuim:
                df_BioFacQuim.append([col, row, data_TSNE.at[row, col]]) 

        X_BioFacQuim = list(df_BioFacQuim[0][2])
        Y_BioFacQuim = list(df_BioFacQuim[1][2])
        S_BioFacQuim= list(df_BioFacQuim[3][2])
        N_BioFacQuim= list(df_BioFacQuim[4][2]) 
                
        #8 Fungi
        Fungi = ["Fungi",]
        df_Fungi = []
        for col in properties:
            for row in Fungi:
                df_Fungi.append([col, row, data_TSNE.at[row, col]]) 
        X_Fungi = list(df_Fungi[0][2])
        Y_Fungi = list(df_Fungi[1][2])
        S_Fungi= list(df_Fungi[3][2])
        N_Fungi= list(df_Fungi[4][2])
        
        #1.NATx
        #ColumnDataSource(data=dict(x=x, y0=y0, y1=y1))
        source1 = ColumnDataSource(dict(x = X_NATx, y = Y_NATx, N = N_NATx))        
        
        
        #2. Marines
        source2 = ColumnDataSource(dict(x = X_Marines, y = Y_Marines, N = N_Marines))
        
        #3 MEGx
        source3 = ColumnDataSource(dict(x = X_MEGx, y = Y_MEGx, N = N_MEGx))
        
        #4.NuBBE
        source4 = ColumnDataSource(dict(x = X_NuBBE, y = Y_NuBBE, N = N_NuBBE))       
        
        #5 Approved
        source5 = ColumnDataSource(dict(x = X_Approved, y = Y_Approved, N = N_Approved)) 
        
        #6. cyanobacterias
        source6 = ColumnDataSource(dict(x = X_Cyanobacterias,y = Y_Cyanobacterias, N = N_Cyanobacterias)) 
             
        #7. Biofacquim
        source7 = ColumnDataSource(dict(x = X_BioFacQuim, y = Y_BioFacQuim, N = N_BioFacQuim)) 
        
         #8 Fungi
        source8 = ColumnDataSource(dict(x = X_Fungi, y = Y_Fungi, N = N_Fungi))
        
        # source9 = ColumnDataSource(dict(x = x_New, y = y_New, N = N_New))
                 
        hover = HoverTool(tooltips = [
            ("PCA1","($x)"),
            ("PCA2","($y)"),
            ("NAME","(@N)"),
            ])

        q = figure(
                title = "Chemical Space by TSNE",
                x_axis_label = " ", y_axis_label=" ",
                x_range = (-7,7), y_range = (-7,7), tools = [hover],  plot_width = 1000, plot_height = 800)
        NATx_plot = q.circle(x = "x", y = "y", source = source1, color="gold", size = 5,  )
        Marines_plot = q.circle(x = "x", y = "y", source = source2, color="blueviolet", size = 5)
        MEGx_plot = q.circle(x = "x", y = "y", source = source3, color="cornflowerblue", size = 5)
        NuBBE_plot = q.circle(x = "x", y = "y", source = source4, color="cyan", size = 5,)
        Approved_plot = q.circle(x = "x", y = "y", source = source5, color="blue", size = 5, )
        Cyanobacterias_plot = q.circle(x = "x", y = "y", source = source6, color="crimson", size = 5, )
        BioFacQuim_plot = q.circle(x = "x", y = "y", source = source7, color="yellow", size = 5,)
        Fungi_plot = q.circle(x = "x", y = "y", source = source8, color="forestgreen", size = 5)
        q.add_tools(LassoSelectTool(), ZoomInTool(), ZoomOutTool(), SaveTool(), PanTool())
        
        legend = Legend(items=[
                ("NATx", [NATx_plot]),
                ("Marines", [Marines_plot]),
                ("MEGx", [MEGx_plot]),
                ("NuBBE", [NuBBE_plot]),
                ("Approved", [Approved_plot]),
                ("Cyanobacterias", [Cyanobacterias_plot]),
                ("BioFacQuim", [BioFacQuim_plot]),
                ("Fungi", [Fungi_plot]),
                ],
                location = "center", orientation = "vertical", click_policy = "hide"
                )
        q.add_layout(legend, place = 'right')
        q.xaxis.axis_label_text_font_size = "20pt"
        q.yaxis.axis_label_text_font_size = "20pt"
        q.xaxis.axis_label_text_color = "black"
        q.yaxis.axis_label_text_color = "black"
        q.xaxis.major_label_text_font_size = "18pt"
        q.yaxis.major_label_text_font_size = "18pt"
        q.title.text_font_size = "22pt"
        
        return p,q


    
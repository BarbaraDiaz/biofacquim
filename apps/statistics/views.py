from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.views import APIView
from bokeh.plotting import figure
from bokeh.embed import components
from datetime import datetime
import pandas as pd
import numpy as np
from .compute_statistics import GenerateStatistics


class StatisticsView(APIView):

    def get(self, request):
        st = GenerateStatistics()
        plot = st.compute_statistics()
        script, div = components(plot)
        return render_to_response('plot_statistics.html', {'script': script, 'div': div})
    







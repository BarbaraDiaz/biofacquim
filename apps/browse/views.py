from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.template import RequestContext
from rest_framework.views import APIView
# from rest_pandas.views import PandasView
from datetime import datetime
import pandas as pd
import numpy as np
import pickle
import os
import glob
import csv
from apps.database.models import NaturalProducts

class BrowseView(APIView):

    def get(self, request):
        df = pd.DataFrame(list(NaturalProducts.objects.all().values('id', 'name', 'reference')))
        # qs = NaturalProducts.objects.all()
        # q = qs.values('id','name','reference')
        print(df)
        return render(request, 'browse.html')

class PlantView(APIView):

    def get(self, request):
        # df = pd.DataFrame(list(NaturalProducts.objects.all().values('id', 'name', 'reference')))
        # data = pd.DataFrame(list(NaturalProducts.objects.all().filter(kingdom='Plant').values()))
        data = pd.DataFrame(list(NaturalProducts.objects.all().filter(kingdom='Plant').values('id', 'name', 'reference', 'year', 'kingdom', 'genus', 'species',  'smiles',  'doi',  'journal',  'site',  'state', 'ic50_1','Activity_1')))
        data_html = data.to_html()
        print(data)
        context = {'loaded_data': data_html}
        return render(request, 'table.html', context)

class FungusView(APIView):

    def get(self, request):
        data = pd.DataFrame(list(NaturalProducts.objects.all().filter(kingdom='Fungus').values('id', 'name', 'reference', 'year', 'kingdom', 'genus', 'species',  'smiles',  'doi',  'journal',  'site',  'state', 'ic50_1','Activity_1')))
        data_html = data.to_html()
        print(data)
        context = {'loaded_data': data_html}
        return render(request, 'table.html', context)

class PropolisView(APIView):

    def get(self, request):
        # df = pd.DataFrame(list(NaturalProducts.objects.all().values('id', 'name', 'reference')))
        # data = pd.DataFrame(list(NaturalProducts.objects.all().filter(kingdom='Plant').values()))
        data = pd.DataFrame(list(NaturalProducts.objects.all().filter(kingdom='Propolis').values('id', 'name', 'reference', 'year', 'kingdom', 'genus', 'species',  'smiles',  'doi',  'journal',  'site',  'state', 'ic50_1','Activity_1')))
        data_html = data.to_html()
        print(data)
        context = {'loaded_data': data_html}
        return render(request, 'table.html', context)


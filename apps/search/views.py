from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, resolve
from django.template import RequestContext
from rest_framework.views import APIView
from datetime import datetime
import pandas as pd
import numpy as np
import pickle
import os
import glob
import csv
from apps.database.models import NaturalProducts
from .forms import InputForm


class SearchView(APIView):

    def post(self, request):
        form = InputForm(request.POST)
        if form.is_valid():
            # letters = form.cleaned_data.get('picked')
            form = form.save(commit=False)
            form.save()
            name_tofind = form.name
            # print(type(name_tofind))
            data = pd.DataFrame(list(NaturalProducts.objects.all().filter(name = name_tofind).values('id', 'name', 'reference', 'year', 'kingdom', 'genus', 'species',  'smiles',  'doi',  'journal',  'site',  'state', 'ic50_1','Activity_1')))
            data_html = data.to_html()
            # print(data)
            context = {'loaded_data': data_html}
            # download_csv = Database.to_csv(route, encoding='utf-8', index = True)
            return render(request, 'table_search.html', context)
        return render(request,'homeperplexity.html',{'form': form})

    def get(self, request):
        form = InputForm()
        return render(request,'homeperplexity.html',{'form': form})

